package com.atlassian.hipchat.model;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.URL;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Model class to keep URL related operators (this acts as a repository)
 *
 * @author Yasitha
 * @version 1.0
 * @since 5 Jun 2016
 */
@EBean
public class LinkModel {

    @RootContext
    Context context;

    private static final int TITLE_LOAD_TIMEOUT = 15000;

    private WebView webView;

    @AfterInject
    void init() {
        webView = new WebView(context);
    }

    private static final String TAG = "LinkModel";

    /**
     * Responsible for extracting title from the given URL
     * Capable of extracting JavaScript generated titles.
     * This implementation relies on native WebView of Android SDK
     * and it would simply acts as a HTTP Client in this context in order to extract the website title
     * If JavaScript generated title is not required you can use getTitleForUrlV2
     *
     * @param url
     * @return Extracted title from the web URL
     * @throws InterruptedException
     */
    public String getTitleForUrl(final String url) throws InterruptedException {

        final StringBuffer title = new StringBuffer();
        final CountDownLatch countDownLatch = new CountDownLatch(1);

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                webView.getSettings().setJavaScriptEnabled(true);
                webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                webView.setWebChromeClient(new WebChromeClient());
                webView.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {

                        title.append(view.getTitle());
                        countDownLatch.countDown();
                    }


                });

                webView.loadUrl(url);
            }
        });


        countDownLatch.await(TITLE_LOAD_TIMEOUT, TimeUnit.MILLISECONDS);

        return title.toString();
    }

    /**
     * This version of title extractor good for pure HTML websites which has a static title
     * This version is not suitable if you need to extract dynamically set title using JavaScript
     * For ex.  https://twitter.com/jdorfman/status/430511497475670016 would only return "Twitter"
     * as the title instead of returning final title set by JavaScript. Therefore, getTitleForUrl method is recommended
     *
     * @param urlStr
     * @return
     * @throws Exception
     */
    public static String getTitleForUrlV2(String urlStr) throws Exception {
        URL url = new URL(urlStr);
        Document doc = Jsoup.parse(url, 3 * 1000);
        String title = doc.title();
        return title;
    }

}
