package com.atlassian.hipchat.model.domain;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Domain class Link
 *
 * @author Yasitha
 * @version 1.0
 * @since 5 Jun 2016
 */
@JsonPropertyOrder({"url", "title"})
public class Link {

    private String url;
    private String title;

    public Link() {
    }

    public Link(String url, String title) {
        this.url = url;
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Link link = (Link) o;

        if (!url.equals(link.url)) return false;
        return title != null ? title.equals(link.title) : link.title == null;

    }

    @Override
    public int hashCode() {
        int result = url.hashCode();
        result = 31 * result + (title != null ? title.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Link{" +
                "url='" + url + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
