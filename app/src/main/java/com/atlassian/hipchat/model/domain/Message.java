package com.atlassian.hipchat.model.domain;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * Domain class Message
 *
 * @author Yasitha
 * @version 1.0
 * @since 5 Jun 2016
 */
@JsonPropertyOrder({"mentions", "emoticons", "links"})
public class Message {

    private List<String> mentions;
    private List<String> emoticons;
    private List<Link> links;

    public List<String> getMentions() {
        return mentions;
    }

    public void setMentions(List<String> mentions) {
        this.mentions = mentions;
    }

    public List<String> getEmoticons() {
        return emoticons;
    }

    public void setEmoticons(List<String> emoticons) {
        this.emoticons = emoticons;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}
