package com.atlassian.hipchat.view;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.atlassian.hipchat.R;
import com.atlassian.hipchat.model.LinkModel;
import com.atlassian.hipchat.presenter.MessagePresenter;
import com.atlassian.hipchat.presenter.MessagePresenterImpl;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NonConfigurationInstance;
import org.androidannotations.annotations.UiThread;

/**
 * Main activity of the application
 *
 * @author Yasitha
 * @version 1.0
 * @since 5 Jun 2016
 */
@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity implements MessageView {

    /**
     * Injected Presenter object
     */
    @NonConfigurationInstance
    @Bean(MessagePresenterImpl.class)
    MessagePresenter presenter;

    private static final String TAG = "MainActivity";


    @AfterViews
    void init() {
        // pass the view object to presenter
        presenter.setView(this);

        // sample message to test, this should be called
        presenter.parseMessage("@bob @john (success) (abcfdsfssfsdfsadfsadfaasdf)such a cool feature; https://twitter.com/jdorfman/status/430511497475670016");

    }


    /**
     * Callback method from the presenter once message parsing is completed.
     * Parsed message will be passed as the parameter
     * @param jsonMessage
     */
    @UiThread
    @Override
    public void setJSONMessage(String jsonMessage) {
        // parsed message will be available, perform the next task based on the parsed message
        Log.d(TAG, "Message parsed: " + jsonMessage);
        Toast.makeText(this, "Message Parsed", Toast.LENGTH_SHORT).show();
    }

    /**
     * Callback method from the presenter if there is parsing error occurred this method will be executed
     * As of now only JSON errors will be passed
     * @param error
     */
    @UiThread
    @Override
    public void setParseError(String error) {
        Log.e(TAG, error);
    }
}
