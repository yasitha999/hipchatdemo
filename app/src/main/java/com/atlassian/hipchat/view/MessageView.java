package com.atlassian.hipchat.view;

/**
 * View component of MVP for messaging
 *
 * @author Yasitha
 * @version 1.0
 * @since 5 Jun 2016
 */
public interface MessageView {

    /**
     * Communicates the corresponding JSON representation of the given message to the View
     *
     * @param jsonMessage
     */
    void setJSONMessage(String jsonMessage);

    /**
     * Any error related to message parsing will be sent to view via this method
     *
     * @param error
     */
    void setParseError(String error);

}
