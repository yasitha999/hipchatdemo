package com.atlassian.hipchat.presenter.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Extracts mentions from the given message
 *
 * @author Yasitha
 * @version 1.0
 * @since 5 Jun 2016
 */
public class MentionsParser implements AbstractParser {


    private static final Pattern MENTIONS_PATTERN = Pattern.compile("^@\\w+|\\s@\\w+");

    @Override
    public List<String> parse(String message) {

        List<String> mentions = new ArrayList<>();

        Matcher mentionMatcher = MENTIONS_PATTERN.matcher(message);

        while (mentionMatcher.find()) {
            String mention = mentionMatcher.group().replace("@", "").trim();
            mentions.add(mention);
        }

        return mentions;
    }
}
