package com.atlassian.hipchat.presenter;

import com.atlassian.hipchat.view.MessageView;

/**
 * Message Presenter interface to define presenter API
 * @author Yasitha
 * @version 1.0
 * @since 5 Jun 2016
 */
public interface MessagePresenter {

    /**
     * Set Message view of the presenter
     * @param view
     */
    void setView(MessageView view);

    /**
     * Parse given message in to JSON format
     * @param message
     */
    void parseMessage(String message);

}
