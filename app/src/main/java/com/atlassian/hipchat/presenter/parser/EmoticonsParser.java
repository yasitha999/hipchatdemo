package com.atlassian.hipchat.presenter.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Extracts Emoticons from the given message
 *
 * @author Yasitha
 * @version 1.0
 * @since 5 Jun 2016
 */
public class EmoticonsParser implements AbstractParser {

    private static final Pattern EMOTICONS_PATTERN = Pattern.compile("(\\()([a-zA-Z0-9]{1,15})(\\))");

    /**
     * Extracts list of Emoticons from the given message
     *
     * @param message
     * @return list of emoticons
     */
    @Override
    public List<String> parse(String message) {
        Matcher emoticonsMatcher = EMOTICONS_PATTERN.matcher(message);
        List<String> emoticons = new ArrayList<>();

        while (emoticonsMatcher.find()) {
            String emo = emoticonsMatcher.group();
            emo = emo.substring(1, emo.length() - 1);
            emoticons.add(emo);
        }
        return emoticons;
    }
}
