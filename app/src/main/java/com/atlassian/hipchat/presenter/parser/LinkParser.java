package com.atlassian.hipchat.presenter.parser;

import android.util.Patterns;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Extract web URLs/Titles from the given message
 *
 * @author Yasitha
 * @version 1.0
 * @since 5 Jun 2016
 */
public class LinkParser implements AbstractParser {

    private static Pattern WEB_URL_PATTERN = Patterns.WEB_URL;

    private static final String TAG = "LinkParser";


    /**
     * Extracts a list of links from the given message
     *
     * @param message
     * @return list of links which include url and the corresponding title of the web page
     */
    @Override
    public List<String> parse(String message) {

        List<String> links = new ArrayList<>();
        Matcher urlMatcher = WEB_URL_PATTERN.matcher(message);

        while (urlMatcher.find()) {

            String url = urlMatcher.group();
            links.add(url);

        }

        return links;
    }
}
