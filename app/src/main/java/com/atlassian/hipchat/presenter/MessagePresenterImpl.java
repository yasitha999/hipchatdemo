package com.atlassian.hipchat.presenter;

import android.util.Log;

import com.atlassian.hipchat.model.LinkModel;
import com.atlassian.hipchat.model.domain.Link;
import com.atlassian.hipchat.model.domain.Message;
import com.atlassian.hipchat.presenter.parser.ParserContext;
import com.atlassian.hipchat.view.MessageView;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Keeps the business logic for message related operations
 *
 * @author Yasitha
 * @version 1.0
 * @since 5 Jun 2016
 */
@EBean
public class MessagePresenterImpl implements MessagePresenter {

    private ObjectMapper objectMapper = new ObjectMapper();

    private MessageView view;

    private static final String TAG = "MessagePresenterImpl";

    /**
     * Injected model
     */
    @Bean
    LinkModel linkModel;

    private ParserContext parserContext = new ParserContext();

    /**
     * Responsible for parsing the message passed from the View
     * Generates parsed JSON representation of the given message and notifies the view
     *
     * @param message
     */
    @Background
    @Override
    public void parseMessage(String message) {

        List<String> emoticons = parserContext.parseMessage(message, ParserContext.ParseStrategy.EMOTICON);
        List<String> mentions = parserContext.parseMessage(message, ParserContext.ParseStrategy.MENTION);
        List<String> links = parserContext.parseMessage(message, ParserContext.ParseStrategy.LINK);

        List<Link> linkObjects = new ArrayList<>();

        for (String url : links) {
            Link linkObj = new Link();
            linkObj.setUrl(url);
            String title = null;
            try {
                title = linkModel.getTitleForUrl(url);
            } catch (InterruptedException e) {
                Log.d(TAG, "Title parsing timeout");
            }
            linkObj.setTitle(title);

            linkObjects.add(linkObj);
        }

        Message messageObj = new Message();

        messageObj.setEmoticons(emoticons);
        messageObj.setMentions(mentions);
        messageObj.setLinks(linkObjects);

        try {

            String messageJSONString = objectMapper.writeValueAsString(messageObj);
            view.setJSONMessage(messageJSONString);

        } catch (JsonProcessingException e) {

            Log.e(TAG, "Message parsing error", e);
            view.setParseError(e.getMessage());

        }
    }

    @Override
    public void setView(MessageView view) {
        this.view = view;
    }


}
