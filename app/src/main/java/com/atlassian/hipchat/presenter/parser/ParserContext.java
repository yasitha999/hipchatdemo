package com.atlassian.hipchat.presenter.parser;

import java.util.List;

/**
 * Parser context class, responsible for parsing message depending on the stratergy
 *
 * @author Yasitha
 * @version 1.0
 * @since 5 Jun 2016
 */
public class ParserContext {

    private AbstractParser messageParser;

    AbstractParser emoticonsParser = new EmoticonsParser();
    AbstractParser mentionsParser = new MentionsParser();
    AbstractParser linkParser = new LinkParser();

    public enum ParseStrategy {
        LINK, MENTION, EMOTICON
    }


    public List<String> parseMessage(String message, ParseStrategy parseStrategy) {

        switch (parseStrategy) {
            case LINK:
                messageParser = linkParser;
                break;
            case MENTION:
                messageParser = mentionsParser;
                break;
            case EMOTICON:
                messageParser = emoticonsParser;
                break;
        }

        List<String> data = messageParser.parse(message);

        return data;
    }

}
