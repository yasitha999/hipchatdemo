package com.atlassian.hipchat.presenter.parser;

import java.util.List;

/**
 * Generic parser for the given message, All parsers must implement this interface
 *
 * @author Yasitha
 * @version 1.0
 * @since 5 Jun 2016
 */
public interface AbstractParser {
    List<String> parse(String message);
}
