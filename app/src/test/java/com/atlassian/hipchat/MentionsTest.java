package com.atlassian.hipchat;

import com.atlassian.hipchat.presenter.parser.MentionsParser;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by Yasitha on 6/5/16.
 */
public class MentionsTest {

    private static MentionsParser mentionsParser;

    @BeforeClass
    public static void setup() {
        mentionsParser = new MentionsParser();
    }

    @Test
    public void testOneMention() {
        List<String> emoticons = mentionsParser.parse("@jhon");
        List<String> expected = Arrays.asList("jhon");
        assertThat(emoticons, is(expected));
    }

    @Test
    public void testOneMentionWithText() {
        List<String> emoticons = mentionsParser.parse("hello @jhon how are you?");
        List<String> expected = Arrays.asList("jhon");
        assertThat(emoticons, is(expected));
    }

    @Test
    public void testMultipleMentions() {
        List<String> emoticons = mentionsParser.parse("@jhon @alex @rod");
        List<String> expected = Arrays.asList("jhon", "alex", "rod");
        assertThat(emoticons, is(expected));
    }

    @Test
    public void testEmpty() {
        List<String> emoticons = mentionsParser.parse("");
        List<String> expected = Arrays.asList();
        assertThat(emoticons, is(expected));
    }

    @Test
    public void testWithOtherSpecialCharacters() {
        List<String> emoticons = mentionsParser.parse("@!#$%^&*()_+");
        List<String> expected = Arrays.asList();
        assertThat(emoticons, is(expected));
    }

    @Test
    public void testSymbol() {
        List<String> emoticons = mentionsParser.parse("@");
        List<String> expected = Arrays.asList();
        assertThat(emoticons, is(expected));
    }

    @Test
    public void testInvalidMention() {
        List<String> emoticons = mentionsParser.parse("@ jhon @");
        List<String> expected = Arrays.asList();
        assertThat(emoticons, is(expected));
    }

    @Test
    public void testEmails() {
        List<String> emoticons = mentionsParser.parse("yasitha999@gmail.com");
        List<String> expected = Arrays.asList();
        assertThat(emoticons, is(expected));
    }


}
