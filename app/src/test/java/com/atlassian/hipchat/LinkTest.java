package com.atlassian.hipchat;

import com.atlassian.hipchat.presenter.parser.LinkParser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by Yasitha on 6/5/16.
 * <p/>
 * RobolectricGradleTestRunner is required as android.util.Patterns.WEB_URL is used when parsing URLs
 * If generic RegEx is used, this test can be executed directly on default Junit test runner.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class LinkTest {

    private LinkParser linkParser;


    @Before
    public void setupInstance() throws Exception {

        linkParser = new LinkParser();
    }

    @Test
    public void testOneURL() {
        String link = "http://www.nbcolympics.com";
        List<String> links = linkParser.parse(link);
        List<String> expected = Arrays.asList("http://www.nbcolympics.com");
        assertThat(links, is(expected));
    }

    @Test
    public void testURLWithText() {
        String link = "hello http://www.nbcolympics.com";
        List<String> links = linkParser.parse(link);
        List<String> expected = Arrays.asList("http://www.nbcolympics.com");
        assertThat(links, is(expected));
    }

    @Test
    public void testEmpty() {
        List<String> links = linkParser.parse("");
        List<String> expected = Arrays.asList();
        assertThat(links, is(expected));
    }

    @Test
    public void testMultipleURL() {
        String link = "hello http://www.nbcolympics.com test www.google.com google.com 192.168.1.1:8080/demo demo app";
        List<String> links = linkParser.parse(link);
        List<String> expected = Arrays.asList("http://www.nbcolympics.com", "www.google.com", "google.com", "192.168.1.1:8080/demo");

        assertThat(links, is(expected));
    }


}
