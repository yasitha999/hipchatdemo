package com.atlassian.hipchat;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Yasitha on 6/5/16.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({EmoticonsTest.class, MentionsTest.class, LinkTest.class})
public class HipChatTestSuite {


}
