package com.atlassian.hipchat;

import com.atlassian.hipchat.presenter.parser.EmoticonsParser;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by Yasitha on 6/5/16.
 */
public class EmoticonsTest {

    private static EmoticonsParser emoticonsParser;

    @BeforeClass
    public static void setup() {
        emoticonsParser = new EmoticonsParser();
    }

    @Test
    public void testOneEmoticon() {
        List<String> emoticons = emoticonsParser.parse("(smile)");
        List<String> expected = Arrays.asList("smile");
        assertThat(emoticons, is(expected));
    }

    @Test
    public void testOneEmoticonWithText() {
        List<String> emoticons = emoticonsParser.parse("hello(smile)");
        List<String> expected = Arrays.asList("smile");
        assertThat(emoticons, is(expected));
    }

    @Test
    public void testMultipleEmoticons() {
        List<String> emoticons = emoticonsParser.parse("(smile)(areyoukiddingme)");
        List<String> expected = Arrays.asList("smile", "areyoukiddingme");
        assertThat(emoticons, is(expected));
    }

    @Test
    public void testMultipleEmoticonsWithText() {
        List<String> emoticons = emoticonsParser.parse("(smile)(areyoukiddingme)hello (lol) hello (o)");
        List<String> expected = Arrays.asList("smile", "areyoukiddingme", "lol", "o");
        assertThat(emoticons, is(expected));
    }

    @Test
    public void testInvalidEmoticon() {
        List<String> emoticons = emoticonsParser.parse("() (@) (#) (@jhon) (smi le) (smi_le)");
        List<String> expected = Arrays.asList();
        assertThat(emoticons, is(expected));
    }

    @Test
    public void testInvalidLength() {
        List<String> emoticons = emoticonsParser.parse("(areyoukiddingmeha)");
        List<String> expected = Arrays.asList();
        assertThat(emoticons, is(expected));
    }

    @Test
    public void testEmpty() {
        List<String> emoticons = emoticonsParser.parse("");
        List<String> expected = Arrays.asList();
        assertThat(emoticons, is(expected));
    }

    @Test
    public void testNumericOnly() {
        List<String> emoticons = emoticonsParser.parse("(1257357)");
        List<String> expected = Arrays.asList("1257357");
        assertThat(emoticons, is(expected));
    }
}
